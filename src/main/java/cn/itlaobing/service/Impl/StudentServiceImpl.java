package cn.itlaobing.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itlaobing.entity.Account;
import cn.itlaobing.entity.Student;
import cn.itlaobing.repository.AccountRepository;
import cn.itlaobing.repository.StudentRepository;
import cn.itlaobing.service.StudentService;
@Service
public class StudentServiceImpl implements StudentService{
	@Autowired
	private StudentRepository studentRepository; 
	@Autowired
	private AccountRepository accountRepository; 
	@Override
	public void allocAccount(Student student, Account account) {
		account.setStudent(student);
		accountRepository.save(account);
		
		student.setAccount(account);
		studentRepository.save(student);
	}

	@Override
	public void save(Student student) {
		studentRepository.save(student);
		
	}

	@Override
	public Student findById(Integer id) {
		return studentRepository.findOne(id);
	}

}
