package cn.itlaobing.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itlaobing.entity.Deptment;
import cn.itlaobing.entity.Employee;
import cn.itlaobing.repository.EmployeeRepository;
import cn.itlaobing.service.EmployeeService;
@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	private EmployeeRepository employeeRepository; 
	@Override
	public void saveEmployeeAndDeptment(Employee employee, Deptment deptment) {
		employee.setDeptment(deptment);
		employeeRepository.save(employee);
		
	}

}
