package cn.itlaobing.service;

import cn.itlaobing.entity.Account;
import cn.itlaobing.entity.Student;

public interface StudentService {
	public void allocAccount(Student student,Account account);
	public void save(Student student);
	public Student findById(Integer id);

}
