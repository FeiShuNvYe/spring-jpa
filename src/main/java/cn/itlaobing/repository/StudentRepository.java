package cn.itlaobing.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.itlaobing.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {

}
