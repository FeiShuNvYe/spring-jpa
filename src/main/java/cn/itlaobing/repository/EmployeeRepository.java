package cn.itlaobing.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.itlaobing.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
