package cn.itlaobing.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.itlaobing.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {

}
