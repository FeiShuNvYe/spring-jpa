package cn.itlaobing.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.itlaobing.entity.Deptment;

public interface DeptmentRepository extends JpaRepository< Deptment, Integer> {

}
