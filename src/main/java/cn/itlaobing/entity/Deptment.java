package cn.itlaobing.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="deptment")
public class Deptment implements Serializable {
	@Id
	@GeneratedValue
	private Integer dept_id;
	private String dept_name;
	@OneToMany(cascade=CascadeType.PERSIST,mappedBy="deptment")
	private List<Employee> employee;
	public Integer getDept_id() {
		return dept_id;
	}
	public void setDept_id(Integer dept_id) {
		this.dept_id = dept_id;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public List<Employee> getEmployee() {
		return employee;
	}
	public void setEmployee(List<Employee> employee) {
		this.employee = employee;
	}
	@Override
	public String toString() {
		return "Deptment [dept_id=" + dept_id + ", dept_name=" + dept_name + ", employee=" + employee + "]";
	}
	
}
