package cn.itlaobing.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="employee")
public class Employee implements Serializable{
	@Id
	@GeneratedValue
	private Integer id;
	private String name;
	@ManyToOne(cascade=CascadeType.PERSIST)
	private Deptment deptment;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Deptment getDeptment() {
		return deptment;
	}
	public void setDeptment(Deptment deptment) {
		this.deptment = deptment;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", deptment=" + deptment + "]";
	}
	
}
