package cn.itlaobing.springjpa.test;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cn.itlaobing.entity.Deptment;
import cn.itlaobing.entity.Employee;
import cn.itlaobing.repository.DeptmentRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class DeptmentTest {
	@Autowired
	private DeptmentRepository deptmentRepository;
	@Test
	public void testSaveDepartment() {
		Deptment department=new Deptment();
		department.setDept_name("市场部");
		
		Employee emp1=new Employee();
		emp1.setName("张三");
		emp1.setDeptment(department);
		
		Employee emp2=new Employee();
		emp2.setName("李四");
		emp2.setDeptment(department);
		
		Employee emp3=new Employee();
		emp3.setName("王五");
		emp3.setDeptment(department);
		
		department.setEmployee(Arrays.asList(emp1,emp2,emp3));
		deptmentRepository.save(department);
	}
}
