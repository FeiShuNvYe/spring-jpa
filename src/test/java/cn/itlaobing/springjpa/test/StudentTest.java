package cn.itlaobing.springjpa.test;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cn.itlaobing.entity.Student;
import cn.itlaobing.repository.StudentRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class StudentTest {
	@Autowired
	private StudentRepository studentRepository;
	@Test
	public void save() {
		Student student=new Student();
		student.setBirth(new Date());
		student.setName("张三");
		studentRepository.save(student);
	}
	@Test
	public void find() {
		List<Student> list=studentRepository.findAll();
		list.forEach(System.out::println);
	}

}
