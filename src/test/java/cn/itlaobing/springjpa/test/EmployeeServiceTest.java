package cn.itlaobing.springjpa.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cn.itlaobing.entity.Deptment;
import cn.itlaobing.entity.Employee;
import cn.itlaobing.service.EmployeeService;
import cn.itlaobing.service.Impl.EmployeeServiceImpl;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class EmployeeServiceTest {
	@Autowired
	private EmployeeService employeeService;
	@Test
	public void SaveEmployeeAndDepartment() {
		Deptment deptment=new Deptment();
		deptment.setDept_name("人事部");
		Employee employee=new Employee();
		employee.setName("君");
		employeeService.saveEmployeeAndDeptment(employee, deptment);
	}
}
