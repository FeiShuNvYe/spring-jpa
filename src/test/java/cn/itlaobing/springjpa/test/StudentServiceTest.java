package cn.itlaobing.springjpa.test;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import cn.itlaobing.entity.Account;
import cn.itlaobing.entity.Student;
import cn.itlaobing.repository.StudentRepository;
import cn.itlaobing.service.StudentService;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class StudentServiceTest {
	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentRepository studentRepository;
	@Test
	public void findById() {
		Integer id=4;
		Student student=studentService.findById(id);
		System.out.println(student.getAccount().getName());
	}
	@Test
	public void testAllocAccount() {
		//创建一个学生
		Student student=new Student();
		student.setName("张三");
		student.setBirth(new Date());
		
		studentService.save(student);
		//查询学生
		Integer id=student.getId();
		student=studentService.findById(id);
		//分配账号
		Account account=new Account();
		account.setName("李四");
		account.setPassword("123456");
		studentService.allocAccount(student, account);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
